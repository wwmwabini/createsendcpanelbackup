#!/opt/alt/python38/bin/python3
#Script to backup and send files to users
#Version 1.0.0 Jan 2024
#By Wallace Mwabini, wallace@ryanada.com

import subprocess, json, getpass, paramiko, smtplib, time, argparse, os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

parser = argparse.ArgumentParser(description="Get cpanel username and client email")

parser.add_argument("--user", type=str, help="Enter cPanel username")
parser.add_argument("--email", type=str, help="Enter client email")
parser.add_argument("--smtppass", type=str, help="SMTP password")
parser.add_argument("--server", type=str, help="Backup server hostname")
parser.add_argument("--serverpass", type=str, help="Backup server password")


args = parser.parse_args()

#Variables

smtp_server = "lon103.truehost.cloud"
smtp_username = "loudgun@truehost.cloud"
#smtp_password = getpass.getpass("Enter sender's email password ")
smtp_password = args.smtppass
smtp_port = 587
bcc_email = "abuse@truehost.cloud"

file_server = args.server

ssh_host=file_server
ssh_username="root"
ssh_port="1624"

cpanel_username = args.user
client_email = args.email
#backup_server_password = getpass.getpass("Enter the password for the server we'll send backup for download: ")
backup_server_password = args.serverpass

print('cpanel_username', cpanel_username)
print('client_email', client_email)
print('backup_server_password', backup_server_password)
print('smtp_password',smtp_password)
print('file_server', file_server)

#Functions


def send_file_to_remote_server(local_file, remote_file, ssh_password):
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(ssh_host, ssh_port, username=ssh_username, password=ssh_password,allow_agent=False,look_for_keys=False)

	remote_file = "/var/www/html/"+remote_file

	sftp = ssh.open_sftp()
	print(local_file)
	print(remote_file)
	sftp.put(local_file, remote_file)
	sftp.close()

	ssh.close()

def send_backup_link(recipient_email, backup_link):

	message_subject = 'Account Backup Complete'
	message_body = '<p>Hello,</p> <p>Please find below your account backup link. </p><p> ' + backup_link + '</p><p>Please download the data to your local computer. The link is valid for 72hrs. </p> <p>Cordially,<br>Truehost Trust and Safety Team</p>'
	
	server = smtplib.SMTP(smtp_server, smtp_port)
	server.set_debuglevel(1)
	server.starttls()
	server.login(smtp_username, smtp_password)

	all_receipients = bcc_email.split(",")+[recipient_email]
	message = MIMEMultipart()
	message['From'] = smtp_username
	message['To'] = recipient_email
	message['Bcc'] = bcc_email
	message['Subject'] = message_subject
	message_body.format(backup_link)
	message.attach(MIMEText(message_body, 'html'))

    # Send the message
	server.sendmail(smtp_username, all_receipients, message.as_string())
	server.quit()

def check_if_backup_is_ready(status_file):
	if cpanel_username in str(status_file['data']['downloads']):
		return True
	else:
		return False

def get_list_of_backups():
	list_downloads_command = 'jetbackup5api -F listDownloads -O json'
	list_downloads = subprocess.check_output(list_downloads_command, shell=True)
	json_list_downloads = json.loads(list_downloads)
	return json_list_downloads


def create_download_queue(parent_id):
	add_queue_command = 'jetbackup5api -F addQueueItems -D "type=4&snapshot_id='+parent_id+'" -O json'
	download_status_file = subprocess.check_output(add_queue_command, shell=True)
	return download_status_file
	
def process_backup():
	for download in json_list_downloads['data']['downloads']:
			if download['account'] == cpanel_username:
				backup_file_full_path = download['full_path']
				backup_file_name=backup_file_full_path.split("/")[-1]

				backup_link = "http://"+file_server+"/"+backup_file_name

				try:
					print("Sending file to storage server...")
					send_file_to_remote_server(backup_file_full_path, backup_file_name, backup_server_password)
					try:
						print("Sending email of backup link...")
						send_backup_link(client_email, backup_link)
					except Exception as e1:
						print("Could not send mail", e1)
				except Exception as e2:
					print("Could not send file to storage server", e2)





#list_of_backups_command = ["jetbackup5api", "-F", "listAccounts", "-O json"]
list_of_backups_command = 'jetbackup5api -F listBackupForAccounts -D "type=1&contains=511" -O json'
#list_of_backups = subprocess.run(list_of_backups_command)
list_of_backups = subprocess.check_output(list_of_backups_command, shell=True)

"""
with open('/home/mwabini/Downloads/output.json', 'r') as file:
    list_of_backups = file.read()
"""
json_list_of_backups = json.loads(list_of_backups)

for acc in json_list_of_backups['data']['accounts']:
	
	if acc['account'] == cpanel_username:
		account_id = acc['backup']['_id']
		parent_id = acc['backup']['parent_id']

		print("Account ID", account_id)
		print("Parent ID", parent_id)

		"""
		with open('/home/mwabini/Downloads/download_status.json', 'r') as file:
			download_status_file = file.read()
		"""
		print("Creating download queue")
		create_download_queue(parent_id)


		json_list_downloads = get_list_of_backups()

		#json_download_status_file = json.loads(create_download_queue(parent_id))

		while check_if_backup_is_ready(json_list_downloads) == False:
			print('check_if_backup_is_ready',check_if_backup_is_ready(json_list_downloads))
			wait_time=180
			print("The backup is not yet ready, sleeping for "+str(wait_time)+" more seconds...")
			time.sleep(wait_time)
			json_list_downloads = get_list_of_backups()
		

		process_backup()
		print("Backup has been uploaded and sent successfully.")
		

				



